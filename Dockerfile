FROM adoptopenjdk/openjdk11:alpine-jre

WORKDIR /opt/app

EXPOSE 8080

ARG JAR_FILE=target/AntifraudFakeService-1.0-SNAPSHOT.jar

COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","app.jar"]






