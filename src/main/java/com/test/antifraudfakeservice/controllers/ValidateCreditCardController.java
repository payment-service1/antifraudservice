package com.test.antifraudfakeservice.controllers;

import com.test.antifraudfakeservice.model.ValidationRequest;
import com.test.antifraudfakeservice.model.ValidationResponse;
import com.test.antifraudfakeservice.services.ValidatePaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController()
@RequiredArgsConstructor
@RequestMapping("antifraud-fake-service/validations")
public class ValidateCreditCardController {

	private final ValidatePaymentService validatePaymentService;

	@PostMapping
	public ResponseEntity<ValidationResponse> validateCreditCard(@RequestBody ValidationRequest validationRequest) {

		return validatePaymentService.validateCreditCard(validationRequest)
									 .map(p -> new ResponseEntity<>(p, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}


}
