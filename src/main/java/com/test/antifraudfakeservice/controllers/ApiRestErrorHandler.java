package com.test.antifraudfakeservice.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@ControllerAdvice
@RequiredArgsConstructor
public class ApiRestErrorHandler {

	@ExceptionHandler(RuntimeException.class)
	public final ResponseEntity<Object> handleException(Exception ex, HttpServletRequest request) {

		log.error("An error occurred: [{}]", buildRequestString(request), ex);

		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private String buildRequestString(HttpServletRequest request) {

		StringBuilder stringBuilder = new StringBuilder();
		Map<String, String> parameters = buildParametersMap(request);

		stringBuilder.append("REQUEST ");
		stringBuilder.append("method=[").append(request.getMethod()).append("] ");
		stringBuilder.append("path=[").append(request.getRequestURI()).append("] ");
		stringBuilder.append("headers=[").append(buildHeadersMap(request)).append("] ");

		if (!parameters.isEmpty()) {
			stringBuilder.append("parameters=[").append(parameters).append("] ");
		}

		return stringBuilder.toString();
	}

	private Map<String, String> buildParametersMap(HttpServletRequest httpServletRequest) {

		Map<String, String> resultMap = new HashMap<>();
		Enumeration<String> parameterNames = httpServletRequest.getParameterNames();

		while (parameterNames.hasMoreElements()) {
			String key = parameterNames.nextElement();
			String value = httpServletRequest.getParameter(key);
			resultMap.put(key, value);
		}

		return resultMap;
	}

	private Map<String, String> buildHeadersMap(HttpServletRequest request) {

		Map<String, String> map = new HashMap<>();

		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
		}

		return map;
	}

}
