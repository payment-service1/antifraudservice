package com.test.antifraudfakeservice.services;

import com.test.antifraudfakeservice.model.ValidationRequest;
import com.test.antifraudfakeservice.model.ValidationResponse;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class ValidatePaymentService {

	private static final String CREDIT_CARD_REJECTED = "4388836889715133";
	private static final String CODE_REJECTED = "01";
	private static final String CODE_APPROVED = "00";
	private static final String MESSAGE_REJECTED = "The credit card was rejected";
	private static final String MESSAGE_APPROEVED = "The credit card is valid";

	public Optional<ValidationResponse> validateCreditCard(ValidationRequest validationRequest) {

		if (Objects.isNull(validationRequest.getNumber()) )
			throw new IllegalArgumentException("The credit card can be valid: " + validationRequest.getNumber());

		if ( validationRequest.getNumber().equals(CREDIT_CARD_REJECTED) ) {
			return getResult(CODE_REJECTED, MESSAGE_REJECTED);
		} else {
			return getResult(CODE_APPROVED, MESSAGE_APPROEVED);
		}

	}

	private Optional<ValidationResponse> getResult(String code, String message) {

		return Optional.of(ValidationResponse.builder()
											 .responseCode(code)
											 .message(message)
											 .build());
	}

}
