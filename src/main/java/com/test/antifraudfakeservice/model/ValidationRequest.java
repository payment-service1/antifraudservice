package com.test.antifraudfakeservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ValidationRequest {

	private String cardHolderName;
	private String number;
	private String expirationDate;
	private String securityCode;
	private String franchise;

}
